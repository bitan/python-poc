"""Program to compute frequency of words in a file.

USAGE: python wordfreq.py filename.txt
"""
import sys

def read_words(filename):
    return open(filename).read().split()

def wordfreq(words):
    """Computes the frequency of each word in the 
    given list of words.
    """
    freq = {}
    for w in words:
        # if w in freq:
        #     freq[w] = freq[w] + 1
        # else:
        #     freq[w] = 1
        freq[w] = freq.get(w, 0) + 1          
    return freq

def print_freq(freq):
    for key,val in freq.items():
        print(key,val)

def main():
    #filename = sys.argv[1]
    filename = 'loop.py'
    words = read_words(filename)
    freq = wordfreq(words)
    print_freq(freq)
    
if __name__ == "__main__":
    main()