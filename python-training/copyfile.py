def copy_file(src, des):
    with open(des, "w") as f:
        f.write(open(src).read())
        
    print ("File copied to ", des)   
        
copy_file("cube.py", "copied_cube.py")