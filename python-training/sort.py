import os

def files(path):
    return sorted(os.listdir(path), key=os.path.getsize)
                  
for file in files('.'):
    print (file)