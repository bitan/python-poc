class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        
    def getx(self):
        return self.x
    
    def display(self):
        print(self.x, self.y)
        
    def add(self, p):
        x = self.x + p.x
        y = self.y + p.y
        return Point(x, y)
    
    def double(self):
        x = self.x * 2
        y = self.y * 2
        return Point(x, y)
    
#p1 = Point(1, 2)
#p2 = Point(10, 20)
#p3 = p1.add(p2)
#p3.display()

p = Point(1,2)
p1 = p.double()
p1.display()

