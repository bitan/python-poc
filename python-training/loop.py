def check_even(n):
    if n%2 == 0:
        print ("even")
    else:
        print ("odd")
        
def check_number(n):
    if n < 10:
        print ("single digit number")
    elif n < 100:
        print ("two digit number")
    else:
        print ("big number")
 
def minimum(x,y):
    if x<y:
        return x        
    return y

def minimum3(x, y, z):
    return minimum(minimum(x, y), z)

def say_hello(name, n):
    for i in range(n):
        print ("Hello", name, i)

def product(numbers):
    product = 1
    for i in numbers:
        product = product * i
    return product 

def factorial(n):
    return product(range(1, n+1))

def squares(numbers):
    squares = []
    for n in numbers:
        squares.append(n*n)
    return squares

def evens(numbers):
    evens = []
    for n in numbers:
        if (n%2 == 0):
            evens.append(n)
    return evens

import os
def list_pyfiles(path):
    return [file for file in os.listdir(path) if file.endswith('.py')]


#print(sum([os.path.getsize(file) for file in os.listdir(".") if file.endswith('.py')]))

def vector_add(x, y):
    return [a+b for a, b in zip(x, y)]
    
print(vector_add([1,2],[3,4]))
        
#print(list_pyfiles("."))    
#print(evens([1,2,3,4,5,6,7,8,9]))   
#print(squares([1,2,3,4]))
#print (factorial(4))
#print(product([1,2,3,4]))
#say_hello("Bitan", 5)
#check_even(23)
#check_number(123)
#print(minimum(13,4))
#print(1+minimum3(111,22,15))    

