import time

class Timer:
    def __init__(self):
        self.tstart = 0
        self.tstop = 0
    
    def start(self):
        self.tstart = time.time()
        
    def stop(self):
        self.tstop = time.time()
        
    def get_time_taken(self): 
        return self.tstop - self.tstart
    

def do_something():
    for i in range(1000):
        for j in range(1000):
            x = i*j

t = Timer()
t.start()
do_something
t.stop
print(t.get_time_taken())


        
    
        