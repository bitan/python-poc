import sys

def cube(n):
    return n*n*n

def main():
    n = int(sys.argv[1])
    print(cube(n))

if __name__ == "__main__":
    main()
    
def test_cube():
    assert(cube(3) == 27)
    assert(cube(2) == 7)
    assert(cube(1) == 1)
    