import re
raw = "| level=INFO | thread=ActiveMQ Session Task-221 | class=com.intuit.platform.services.common.logging.framework.LogScrubber | Dequeued BRM Event From BRM AQ:dence Queue', u'[txid=1f360300-0694-11e8-96c7-005056856e01 ,docId=123147066102099 ,app=obill-event-manager ,msgPayload=<?xml versi'"
splunk = 'LogScrubber.\s+\|+\s.:'
search_fields = splunk.split(',')
for search_field in search_fields:
    # Parse search fields to get field, pre-delimiter and post-delimiter
    field = search_field.split('.')
    if re.findall(field[0], raw):
        print field[0]
        print field[1]
        print field[2]
        splunk_field = re.findall(r".*{0}{1}(.*?){2}".format(field[0],field[1],field[2]),raw)
        print splunk_field