from Crypto.Cipher import AES
import binascii

# Convert values from hex to binary
key = binascii.unhexlify('090741b3a0c15fa2446f0b1b0bfa89a0')
iv = binascii.unhexlify('24c67a98f7aa5349d86ae58a25c21db2')
values = binascii.unhexlify('337ba798b8105cd90c53004ca5c47bbb7365b2ea6ee15119bee5de3ee826356492ea920df1c6f09f5cb649eb9ab53b2dcffc40819c9730cf8439bb28efdf4636c0392d447814cec4bbcdee7026fa7f118d286b539d7a575f40bb046d60f44128a04d793f4aa5d4a3e3e591fd1761e9db') 

# Decrypt aes-128-cbc encrypted string
decryptor = AES.new(key, AES.MODE_CBC, iv)
plain = decryptor.decrypt(values)

# Decode decrypted values to utf-8
parameters = plain.decode('utf-8', 'ignore').split('|')

# Parse results and add to dict
results = {}
for param in parameters:
    item = param.split('=')
    results[item[0]] = item[1]

# 'results' dict will have all user parameters in key-value pair
print (results)
