# Library to implement rate limit
# Method - True if request is success and false if the no of request > 5
from __builtin__ import False
from datetime import datetime, timedelta
# Input argument

# Set Global variable - Keep a count of method invocation
# Method - Check the count of the requests
# Return true or false based on counter

global requests, time
requests = 0
time = datetime.now()

# Function to rate limit
def rate_limit(): 
    # Check time
    current_time = datetime.now()
    
    delta = (current_time - time).total_seconds()
    print delta 
    
    if delta <= 0.000202:
        global requests, time
        if requests > 4:
            #print "Limit reached"
            return False
        
        requests += 1
        #print "Limit NOT reached yet"
        return True
    else:
        time = datetime.now()
        requests = 0
        return False


for i in range(100):
    print rate_limit(), requests



