import json 
import os
import itertools
from scipy.stats.stats import ss
import collections

foo = ['hi']
print(foo)
# Output: ['hi']

bar = foo
bar += ['bye']
print bar
print(foo)

def repeatedString(s, n):
    while len(s) < n:
        s += s
    s = s[:n]
    print s.count('a')
    return s.count('a')

#repeatedString("aba", 10)
#repeatedString("a", 1000000000000)

def jumpingOnClouds1(c):
    l = []
    for i in range(len(c)):
        l.append(i)
        if c[i] == 1:
            l.remove(i)
    jumps = 0
    j = 0
    ls = len(l)
    print l
    print ls

    while j<ls-1:
        if (j+2)<ls:
            if l[j+2] - l[j] == 2:
                j = j+2
                jumps += 1
            else:
                j = j+1
                jumps += 1
        else:
            jumps += 1
            j+=1
            
    print jumps
    return jumps

def jumpingOnClouds(c):
    n = len(c)
    res = 0
    i = 0
    while i < n-1:
        if i+2<n and c[i+2] == 0:
            i = i+2
            res += 1
        else:
            i = i+1
            res += 1
    print(res)
    return res

#jumpingOnClouds1([0,1,0,0,0,0,0,1,0,1,0,0,0,1,0,0,1,0,1,0,0,0,0,1,0,0,1,0,0,1,0,1,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,1,0,1,0,1,0,1,0,0,0,0,0,0,1,0,0,0])
 

def sum_of_hg(l):
    
    s = []
    for i in range(len(l)):
        for j in range(len(l[i])):
            if i == 0:
                s.append(l[i][j])
            if i == 1 and j == 1:
                s.append(l[j][j])
            if i == 2:
                s.append(l[i][j])
     
    sum = reduce((lambda x, y: x + y), s)
    return sum
 
 
def divide_matrix(m):
    s = []
    sum = []
    
    for i in range(len(m)):
        s.append(m[i][0:3])
    for i in range(len(m)):
        s.append(m[i][1:4])
    for i in range(len(m)):
        s.append(m[i][2:5])
    for i in range(len(m)):
        s.append(m[i][3:6])
    
    for i in range(len(m)):
        if i>0 and i<4:
            s.append(m[i][0:3])
    
    for i in range(len(m)):
        if i>0 and i<4:
            s.append(m[i][1:4])
    
    for i in range(len(m)):
        if i>0 and i<4:
            s.append(m[i][2:5])
    
    for i in range(len(m)):
        if i>0 and i<4:
            s.append(m[i][3:6])
            
    for i in range(len(m)):
        if i>0 and i<4:
            s.append(m[i][0:3])
    
    for i in range(len(m)):
        if i>1 and i<5:
            s.append(m[i][1:4])
    
    for i in range(len(m)):
        if i>1 and i<5:
            s.append(m[i][2:5])
    
    for i in range(len(m)):
        if i>1 and i<5:
            s.append(m[i][3:6])
    
    f = []
    for v in range(1, len(s)+1):
        if v%3 == 0:
            f.append(s[v-1])
            sum.append(sum_of_hg(f))
            f = []
        else:
            f.append(s[v-1])
    return max(sum)
    
#divide_matrix([[1, 1, 1, 0, 0, 0], [0, 1, 0, 0, 0, 0], [1, 1, 1, 0, 0, 0], [0, 0, 2, 4, 4, 0], [0, 0, 0, 2, 0, 0], [0, 0, 1, 2, 4, 0]])

 
 

def sublist(l):
    t = permutations(l)
    c=0
    for i in list(t):
       print i
       c+=1
    print c
#sublist([3,7,4,6,5]) 


def sum(*argv):
    sum = 0
    for arg in argv:
        sum  = sum + arg
    return sum

def adj(l):
    fl = []
    n = len(l)
    for i in range(n-1):
        if l[i+1] - l[i] == 1:
            if l in fl:
                fl.remove(l)
            break
        else:
            if l not in fl:
                fl.append(l) 
    return fl

#p = [3,-7,-4,6,-5,3,-7,-4,6,-5] 
def adjset():
    p = [8006]
    n = []
    for i in range(2, len(l)-1):
        n.extend(list(itertools.combinations(l, i)))
    
    final = []
    for j in n:
        if adj(j):
            final.append(adj(j))
                
    zz = str(final)
    ss = ""
    
    ss = zz.replace("(","").replace(")","")
    dd = ""
    ff = ""
    suml = []

    for bb in ss.split("],"):
        dd = bb.replace("[","").replace(" ","").replace("]","")
        #print dd
    
        fll = []
        for m in dd.split(","):
            for c,val in enumerate(p):
                if str(c) in m:
                    ff = m.replace(str(c), str(val)) 
                    fll.append(int(ff))
        print fll
        sum = 0
        for ss in fll:
            sum = sum + ss
        
        suml.append(sum)
        
    print max(suml)

def bubbleSort(array):
    count = 0
    #print("array is currently",array)
    for idx in range(len(array)-1):
        if array[idx] > array[idx + 1]:
            array[idx],array[idx + 1] = array[idx + 1],array[idx]
            count += 1
            #print("swaped and count is currently",count)
            #print("array is currently",array)
    if count == 0:
        #print("Count is zero")
        #print("array is currently",array)
        return array
    else:
        #print("Count is not zero")
        print array
        return bubbleSort(array)
    
#bubbleSort([6, 3, 9, 8, 1, 0, 12, 13])

def testd():
    d = {}
    d["1"] = "Test"
    d["2"] = "Test2"
    
    for k,v in d.items():
        print k,v
    
    for k in d.keys():
        print k
    
    for v in d.values():
        print v

def freq(text, a):
    return str(text).count(str(a))

#print freq([6, 3, 9, 8, 6, 1, 0, 12, 13],6)

def swapcase(text):
    if text in text.lower():
        return text.upper()
    return text.lower()

print swapcase("test")




    
   
    
