
#Count the no of votes
#IP Arg -  List of STRING -  Name of the person who gets voted for
#Return the person with max no of votes


# Take the list of string as ipput
# Parse the list, to get the entries and store its count in a dict
# Iterate the values and get the max 

# Count votes for a candidate
def countVotes(candidates):
    
    # Initialize the result dict
    result = {}
    max = 0
    winner = ""
    
    if len(candidates) < 1:
        return
    
    # Parse the candidate list
    for candidate in candidates:
        count = candidates.count(candidate)
        if count > max:
            max = count
            winner = candidate
        else:
            max = 0
            
    return winner
    
    #print max(result)
     

if countVotes(["Mary", "Bob", "Mary", "Gary", "Bob", "Bob", "Bob"]) is not "Bob":
    print "ERROR 1"

if countVotes(["Mary", "Bob", "Mary", "Gary", "Bob"]) is not "Bob":
    print "ERROR 2" 
    
if countVotes(["Mary", "Bob", "Mary", "Gary", "Bob", "Bob", "Mary", "Gary", "Gary"]) is not "Gary":
    print "ERROR 3" 

if countVotes([]) is not None:
    print "ERROR 4" 