
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class Parse {

	/*public static void main(String[] args) throws Exception 
	{ 
		File file = new File("test1.conf"); 

		BufferedReader br = new BufferedReader(new FileReader(file)); 
		StringBuffer sb = new StringBuffer();
		String[] aliases = null;
		String[] servers = null;
		String[] redirects = null;
		List<RewriteRule> rewriteRuleList = new ArrayList<RewriteRule>();
		RewriteRule rewriteRule;

		String st; 
		while ((st = br.readLine()) != null) 	  
			if (st.contains("<VirtualHost"))	 {
				sb = new StringBuffer();
			} else if (st.contains("ServerName"))	{
				if(st!=null) {
					servers = st.trim().replaceAll("\\s+", " ").split(" ");				
				}

			} else if (st.contains("RewriteRule"))	{
				rewriteRule = new RewriteRule();
				redirects = st.trim().replaceAll("\\s+", " ").split(" ");
				rewriteRule.setField1(redirects[1]);
				rewriteRule.setField2(redirects[2]);
				rewriteRule.setField3(redirects[3]);
				rewriteRuleList.add(rewriteRule);

			} else if (st.contains("ServerAlias"))	{
				if(st!=null) {
					aliases = st.trim().replaceAll("\\s+", " ").split(" ");				
				}

			} else if (st.contains("</VirtualHost"))	 {
				for (int i = 1; i < servers.length; i++)   {
					for (RewriteRule rewriteRules : rewriteRuleList) {
						System.out.println(servers[i] + ",," + rewriteRules.getField1() + "," + rewriteRules.getField2() + ",\"" + rewriteRules.getField3() + "\"");
					}
					if (aliases != null) {
						for (int j = 1; j < aliases.length; j++) { 
							for (RewriteRule rewriteRules : rewriteRuleList) {
								System.out.println(servers[i] + "," + aliases[j] + "," + rewriteRules.getField1() + "," + rewriteRules.getField2() + ",\"" + rewriteRules.getField3() + "\"");
							}
						}
					}
				}					

				aliases = null;
				servers = null;
				redirects = null;
				rewriteRule = null;
				rewriteRuleList.clear();
			} 
	}*/
	
	/*public static void main(String[] args) throws Exception  {
		File file = new File("test2.conf"); 

		BufferedReader br = new BufferedReader(new FileReader(file)); 
		StringBuffer sb = new StringBuffer();
		String st; 
		String[] servers = null;
		String[] redirects = null;
		
		while ((st = br.readLine()) != null) 
			if (st.contains("RewriteCond"))	 {
				sb = new StringBuffer();
				servers = st.trim().replaceAll("\\s+", " ").replaceAll("\\^","").replaceAll("\\\\","").split(" ");
				
			} else if (st.contains("RewriteRule"))	{
				if(st!=null) {
					redirects = st.trim().replaceAll("\\s+", " ").split(" ");
					System.out.println(servers[2] + ",," + redirects[1] + "," + redirects[2] + ",\"" + redirects[3] + "\"");
					servers = null;
					redirects = null;
				}
			}
	}*/
	
	public static void main(String[] args) throws Exception  {
		File file = new File("test.txt"); 
		BufferedReader br = new BufferedReader(new FileReader(file)); 
		String st;
		String[] line = null;
		List list = new ArrayList();
		StringBuffer f = new StringBuffer();
		StringBuffer k = new StringBuffer();
		while ((st = br.readLine()) != null) {
			st = st.trim();
			if (st.contains("\t")) {
				line = st.split("\t");
				f.append(line[0]);
				k.append(line[1]);
			} else {
				k.append(","+st);
			}
					
			if (st.trim().isEmpty()) {
				list.add(f + ":" + k);
				f = new StringBuffer();
				k = new StringBuffer();
			}
			
		}
		
		for (int i = 0; i < list.size(); i++) {
			System.out.println(((String) list.get(i)).replaceAll(",$", ""));
		}
		
		//System.out.println(list.size());
	}
}